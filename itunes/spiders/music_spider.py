# -*- coding: utf-8 -*-
import json
from re import findall
from urllib.parse import unquote

import scrapy
from scrapy.http import TextResponse
from scrapy.spiders import SitemapSpider

from itunes.items import AlbumItem, SongItem


class MusicSpiderSpider(SitemapSpider):
    name = 'music_spider'
    sitemap_rules = [
        ('/album', 'parse_album'),
    ]
    sitemap_urls = [
        'http://sitemaps.itunes.apple.com/sitemap_index_41.xml'
    ]

    def parse_album(self, response):
        request_url = response.request.url.split("/")[-1]
        album_id = findall(r"\d+", request_url)[0]

        print("Parsing Album: %s" % album_id)
        yield scrapy.Request(f"https://itunes.apple.com/us/lookup?id={album_id}", callback=self.parse_album_info)

        raw_web_config = response.css("meta[name='web-experience-app/config/environment']::attr(content)") \
            .extract_first()
        web_config = json.loads(unquote(raw_web_config))
        bearer_token = web_config['MEDIA_API']['token']
        headers = {
            'authorization': f'Bearer {bearer_token}',
            'Referer': request_url
        }

        yield scrapy.Request(f"https://amp-api.music.apple.com/v1/catalog/US/albums?ids={album_id}",
                             callback=self.parse_track, meta={'config': web_config}, headers=headers)

    def parse_track(self, response: TextResponse):
        body = json.loads(response.body_as_unicode())
        data = body['data']
        if len(data) > 0:
            for data_type in data:
                tracks = data_type['relationships']['tracks']['data']
                for track in tracks:
                    track_id = track['id']
                    self.logger.info('Parsing Track: %s' % track_id)
                    yield scrapy.Request(f"https://itunes.apple.com/us/lookup?id={track_id}",
                                         callback=self.parse_album_track)

    def parse_album_track(self, response):
        body = json.loads(response.body_as_unicode())
        results = body.get("results")
        if len(results) > 0:
            json_response = results[0]

            album = SongItem()
            for k in json_response.keys():
                album[k] = json_response[k]

            yield album

    def parse_album_info(self, response: TextResponse):
        body = json.loads(response.body_as_unicode())
        results = body.get("results")
        if len(results) > 0:
            json_response = results[0]

            album = AlbumItem()
            for k in json_response.keys():
                album[k] = json_response[k]

            yield album
